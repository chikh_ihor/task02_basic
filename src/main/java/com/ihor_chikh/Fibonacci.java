package com.ihor_chikh;

import java.util.ArrayList;
import java.util.List;

/**
 * Fibonacci set object.
 * Overrides basic methods to work with set
 * (which are described in task 2) from Set.
 *
 * @author Chikh Ihor
 * @see Set
 */

public final class Fibonacci extends Set {
    /**
     * contains size of fibonacci set.
     */
    private int setSize;

    /**
     * Constructor.
     *
     * @param size (required) size of fibonacci set.
     */
    Fibonacci(final int size) {
        super(fillNumbers(size));
        this.setSize = size;
    }

    /**
     * method fills set with fibonacci numbers.
     *
     * @param size (required) represents numbers quantity in the set.
     * @return fibonacci set.
     */
    private static List fillNumbers(final int size) {
        List<Integer> list = new ArrayList<>();
        if (size >= 2) {
            list.add(0);
            list.add(1);
            for (int i = 0; i < size - 2; i++) {
                int j = i + 1;
                list.add(list.get(i) + list.get(j));
            }
        }
        if (size == 1) {
            list.add(0);
        }
        return list;
    }

    @Override
    public void print() {
        System.out.println("Fibonacci numbers: " + "\n"
                + "N: " + setSize);
        super.print();
    }
}

