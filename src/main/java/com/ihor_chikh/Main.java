package com.ihor_chikh;

import java.util.Scanner;

/**
 * @author Chikh Ihor
 */

public class Main {

    /**
     * entry point.
     *
     * @param args args
     */
    public static void main(final String[] args) {
        Scanner scanner = new Scanner(System.in, "UTF-8");
        System.out.print("Input N to create Fibonacci set: ");
        int fibonacciSet = scanner.nextInt();
        Set set = new Fibonacci(fibonacciSet);
        set.print();
        System.out.print("Input the beginning of the interval: ");
        int from = scanner.nextInt();
        System.out.print("Input the end of the interval: ");
        int to = scanner.nextInt();
        set = new Interval(from, to);
        set.print();
    }
}
