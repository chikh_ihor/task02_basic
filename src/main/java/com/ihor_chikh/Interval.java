package com.ihor_chikh;

import java.util.ArrayList;
import java.util.List;

/**
 * Sequence of integers.
 */
public final class Interval extends Set {
    /**
     * The beginning of the interval.
     */
    private int start;
    /**
     * End of interval.
     */
    private int end;

    /**
     * Constructor.
     * Create an Interval object.
     *
     * @param from (required) the beginning of the interval.
     * @param to   (required) end of interval.
     */
    public Interval(final int from, final int to) {
        super(fillNumbers(from, to));
        this.start = from;
        this.end = to;
    }

    /**
     * @param from (required) the beginning of the interval.
     * @param to   (required) end of interval.
     * @return sequence of integers.
     */
    private static List fillNumbers(final int from, final int to) {
        List<Integer> list = new ArrayList<>();
        for (int i = from; i <= to; i++) {
            list.add(i);
        }
        return list;
    }

    @Override
    public void print() {
        System.out.println("Interval: " + "\n"
                + "[" + start + ", " + end + "]");
        super.print();
    }
}
