package com.ihor_chikh;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class Set.
 * <p>
 * Represents basic methods to work with set (which are described in task 2).
 * Set should be extended.
 *
 * @author Chikh Ihor
 * @version 1.0
 */
abstract class Set {

    /**
     * contains the list of numbers.
     */
    private List<Integer> set;
    /**
     * contains the list of odd numbers.
     * subset of set.
     */
    private List<Integer> oddNumbers;
    /**
     * contains the list of even numbers.
     * subset of set.
     */
    private List<Integer> evenNumbers;
    /**
     * contains sum of odd numbers.
     */
    private int sumOddNumbers;
    /**
     * contains sum of even numbers.
     */
    private int sumEvenNumbers;
    /**
     * contains the biggest odd number.
     */
    private int theBiggestOddNumber;
    /**
     * contains the biggest even number.
     */
    private int theBiggestEvenNumber;
    /**
     * contains percentage of odd numbers in set list.
     */
    private int oddPercentage;
    /**
     * contains percentage of even numbers in set list.
     */
    private int evenPercentage;


    /**
     * Constructor.
     *
     * @param list Description will be added coon
     */
    Set(final List list) {
        this.set = new ArrayList<>(list);
        this.oddNumbers = getOddNumbers(set);
        this.evenNumbers = getEvenNumbers(set);
        this.sumOddNumbers = getSum(oddNumbers);
        this.sumEvenNumbers = getSum(evenNumbers);
        this.theBiggestOddNumber = getTheBiggestNumber(oddNumbers);
        this.theBiggestEvenNumber = getTheBiggestNumber(evenNumbers);
        this.oddPercentage = getPercentage(set, oddNumbers);
        this.evenPercentage = getPercentage(set, evenNumbers);
    }

    /*public void setSet(List set) {
        this.set = set;
        this.oddNumbers = getOddNumbers(this.set);
        this.evenNumbers = getEvenNumbers(this.set);
        this.sumOddNumbers = getSum(oddNumbers);
        this.sumEvenNumbers = getSum(evenNumbers);
        this.theBiggestOddNumber = getTheBiggestNumber(oddNumbers);
        this.theBiggestEvenNumber = getTheBiggestNumber(evenNumbers);
        this.oddPercentage = getPercentage(set, oddNumbers);
        this.evenPercentage = getPercentage(set, evenNumbers);
    }*/

    /**
     * getter.
     * Return set passed to the constructor.
     *
     * @return set Description will be added coon
     */
    public List getSet() {
        return set;
    }

    /**
     * getter.
     * Return odd numbers. Subset from set set.
     *
     * @return odd numbers set.
     */
    public List getOddNumbers() {
        return oddNumbers;
    }

    /**
     * getter.
     * Return even numbers. Subset from set set.
     *
     * @return even numbers set.
     */
    public List getEvenNumbers() {
        return evenNumbers;
    }

    /**
     * getter.
     * Return odd numbers sum.
     *
     * @return odd numbers sum.
     */
    public int getSumOddNumbers() {
        return sumOddNumbers;
    }

    /**
     * getter.
     * Return even numbers sum.
     *
     * @return even numbers sum.
     */
    public int getSumEvenNumbers() {
        return sumEvenNumbers;
    }

    /**
     * getter.
     * Return percentage of odd numbers.
     *
     * @return percentage of odd numbers.
     */
    public int getOddPercentage() {
        return oddPercentage;
    }

    /**
     * getter.
     * Return percentage of even numbers.
     *
     * @return percentage of even numbers.
     */
    public int getEvenPercentage() {
        return evenPercentage;
    }

    /**
     * getter.
     * Return the biggest even number.
     *
     * @return the biggest even number.
     */
    public int getTheBiggestEvenNumber() {
        return theBiggestEvenNumber;
    }

    /**
     * getter.
     * Return the biggest odd number.
     *
     * @return the biggest odd number.
     */
    public int getTheBiggestOddNumber() {
        return theBiggestOddNumber;
    }

    /**
     * Description will be added coon.
     *
     * @param set Description will be added coon.
     * @return set of even numbers.
     */
    public static List getOddNumbers(final List set) {
        List<Integer> oddNumbers = new ArrayList<>();
        for (int i = 0; i < set.size(); i++) {
            if (isOdd((int) set.get(i))) {
                oddNumbers.add((int) set.get(i));
            }
        }
        return oddNumbers;
    }

    /**
     * Description will be added coon.
     *
     * @param list Description will be added coon
     * @return Description will be added coon
     */
    public static List getEvenNumbers(final List list) {
        List<Integer> evenNumbers = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (isEven((int) list.get(i))) {
                evenNumbers.add((int) list.get(i));
            }
        }
        return evenNumbers;
    }

    /**
     * Description will be added coon.
     *
     * @param number Description will be added coon.
     * @return Description will be added coon
     */
    public static boolean isOdd(final Integer number) {
        return number % 2 == 1;
    }

    /**
     * Description will be added coon.
     *
     * @param number Description will be added coon
     * @return Description will be added coon
     */
    public static boolean isEven(final Integer number) {
        return number % 2 == 0;
    }

    /**
     * Description will be added coon.
     *
     * @param set Description will be added coon
     * @return Description will be added coon
     */
    public static List doReverse(final List set) {
        List<Integer> reversedList = new ArrayList<>();
        for (int i = set.size() - 1; i >= 0; i--) {
            reversedList.add((int) set.get(i));
        }
        return reversedList;
    }

    /**
     * Description will be added coon.
     *
     * @param set Description will be added coon
     * @return Description will be added coon
     */
    public static int getSum(final List set) {
        int sum = 0;
        for (int i = 0; i < set.size(); i++) {
            sum = sum + (int) set.get(i);
        }
        return sum;
    }

    /**
     * Description will be added coon.
     *
     * @param set Description will be added coon
     * @return Description will be added coon
     */
    public static int getTheBiggestNumber(final List set) {
        int result = (int) set.get(0);
        for (int i = 0; i < set.size(); i++) {
            if (result < (int) set.get(i)) {
                result = (int) set.get(i);
            }
        }
        return result;
    }

    /**
     * Description will be added coon.
     *
     * @param mainList   Description will be added coon
     * @param nestedList Description will be added coon
     * @return Description will be added coon
     */
    public static int getPercentage(final List mainList,
                                    final List nestedList) {
        final int oneHundred = 100;
        return (nestedList.size() * oneHundred) / mainList.size();
    }

    /**
     * prints info about set.
     */
    public void print() {
        System.out.println("Set: " + set + "\n"
                + "Odd numbers: " + oddNumbers + "\n"
                + "Even numbers (reversed): " + doReverse(evenNumbers) + "\n"
                + "Odd numbers sum: " + sumOddNumbers + "\n"
                + "Even numbers sum: " + sumEvenNumbers + "\n"
                + "The biggest odd number: " + theBiggestOddNumber + "\n"
                + "The biggest even number: " + theBiggestEvenNumber + "\n"
                + "Odd percentage: " + oddPercentage + "\n"
                + "Even percentage: " + evenPercentage + "\n"
        );
    }

    @Override
    public String toString() {
        return set.toString();
    }
}
